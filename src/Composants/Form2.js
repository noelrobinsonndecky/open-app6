// Les formulaires non-controlés

// Ma fonction handleSubmit
const handleSubmit = (e) => {
    e.preventDefault()
    alert(e.target['monInput'].value);
}


const Form2 = () => {
    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input type="text" name="monInput" id="monInput" defaultValue="Votre texte"/>
                <br/>
                <br/>
                <button type="submit">Entrer</button>
            </form>
        </div>
    );
};

export default Form2;