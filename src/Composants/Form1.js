// L'entéte

import Form2 from "./Form2";
import Form3 from "./Form3";

const Form1 = () => {
    return (
        <div>
            <h1>Les formulaires en React</h1>
            <p>
                En React, la gestion des formulaires est simplifiée : on a accès à la valeur très facilement, 
                qu'il s'agisse d'un input checkbox, d'un textarea, ou encore d'un select avec  onChange.
                Il existe deux grandes manières de gérer les        : 
                la manière contrôlée et la manière non contrôlée.
                React encourage l'utilisation des formulaires contrôlés.

            </p>
            <br/>
            <h2>Les formulaires non controlés</h2>
                <Form2/>
            <p>
                Effectivement, les formulaires non contrôlés nous permettent de ne pas avoir 
                à gérer trop d'informations. Mais cette approche est un peu moins "React", 
                parce qu'elle ne permet pas de tout faire.
            </p>
            <br/>
            <h2>Les Formulaires controlés</h2>
            <p>
                Ici, pour vous montrer l'utilisation des formulaires contrôlés, je vais avoir besoin d'une notion:
                le state (état).
                Le state local nous permet de garder des informations. 
                Ces informations sont spécifiques à un composant et elles proviennent d'une interaction 
                que l'utilisateur a eue avec le composant.
                Donc je vais créer ma variable  inputValue   
                et la fonction qui va permettre de changer sa valeur dans le state local avec  useState.
            </p>
            <br/>
            <Form3/>
            <h4>Avantage des formulaires controlés</h4>
            <p>
                Permet d'interagir directement avec la donnée renseignée par l'utilisateur. 
                Vous pouvez donc afficher un message d'erreur si la donnée n'est pas valide, 
                ou bien la filtrer en interceptant une mauvaise valeur.
            </p>
            <br/>
            
            
        </div>
    );
};

export default Form1;