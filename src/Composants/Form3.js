// Les formulaires controlés

import { useState } from "react";


const Form3 = () => {
    // Ma variable inputValue
    const[inputValue,setInputValue] = useState("Votre question Ici");


    return (
        <div>
            <form>
                <textarea
                    value={inputValue}
                    onChange={(e) => setInputValue(e.target.value)}
                />
                <br/>
                <button type="submit" onClick={() => alert(inputValue)}>Alertez-moi</button>
            </form>
        </div>
    );
};

export default Form3;


// Les hooks
// useState

/**
 * useState   est un hook qui permet d’ajouter le state local React à des composants fonctions.
 * 
 * Un hook est une fonction qui permet de « se brancher » (to hook up) sur des fonctionnalités React. 
 * On peut d'ailleurs les importer directement depuis React. 
 * 
 * useState   nous renvoie une paire de valeurs dans un tableau de 2 éléments, 
 * que nous récupérons dans les variables  cart   et  updateCart   dans notre exemple. 
 * Le premier élément est la valeur actuelle, et le deuxième est une fonction qui permet de la modifier.
 * 
 * Initialisez votre state
 * 
 * Le paramètre passé entre parenthèses à useState :  useState(""). 
 * il correspond à l'état initial de notre state. Cet état initial peut être un nombre comme ici, 
 * une string, un booléen, un tableau ou encore un objet avec plusieurs propriétés.
 * 
 * En résumé:
 * 
 * Le state local est présent à l’intérieur d’un composant : 
 * ce composant peut être re-render autant de fois que l'on veut, mais les données seront préservées.
 * Un hook est une fonction qui permet de « se brancher » (to hook up) sur des fonctionnalités React.
 * useState   est un hook qui permet d’ajouter le state local React à des fonctions composants :
 * Il nous renvoie une paire de valeurs dans un tableau de 2 valeurs, récupérée dans les variables entre crochets.
 * Il faut initialiser votre state avec un paramètre passé entre parenthèses – 
 * un nombre, une string, un booléen, un tableau ou même un objet.
 * 
 * Partage de State dans différents composants
 * 
 * Pour utiliser un même état entre plusieurs composants, il faut :
 * faire remonter l'état dans le composant parent commun le plus proche ;
 * puis faire descendre la variable d'état et la fonction pour mettre à jour cet état dans des props.
 * 
 * Déclenchez des effets avec useEffect
 * 
 * Mais comment faire si on veut effectuer une action qui ne fait pas partie du return ? 
 * Qui intervient après que React a mis à jour le DOM ? 
 * Par exemple, si vous voulez déclencher une alerte à chaque fois que votre panier est mis à jour ? 
 * Ou bien même pour sauvegarder ce panier à chaque mise à jour ?
 * 
 * Ces types d'actions s'appellent des effets de bord, et pour cela, nous avons  useEffect. 😎 
 * Ils nous permettent d'effectuer une action à un moment donné du cycle de vie de nos composants.
 * 
 * useEffect:
 * 
 * permet d'effectuer notre effet une fois le rendu du composant terminé. 
 * Et comme  useEffect  est directement dans notre composant, nous avons directement accès à notre state, 
 * à nos variables, nos props, magique n'est-ce pas ?
 *
 * Préciser quand déclencher uneffet sur un tableau de dépendance:
 * Petit rappel : le premier paramètre passé à useEffect est une fonction.
 * 
 * Cette fonction correspond à l'effet à exécuter. Ici, il s'agit de :
 * useEffect() => {
 *     alert(`J'aurai ${total}€ à payer 💸`
 * }
 * Le deuxième paramètre de  useEffect   accepte un tableau noté entre crochets : 
 * il s'agit du tableau de dépendances.
 * 
 * 
*/